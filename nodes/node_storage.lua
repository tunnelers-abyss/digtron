-- internationalization boilerplate
local S = digtron.S
-- local MP = minetest.get_modpath(minetest.get_current_modname())
-- local S = dofile(MP.."/intllib.lua")

local LOCKED_FLAG_NAME = "locked"
local OWNER_ONLY_FLAG_NAME = "owner_only"
local OWNER_FIELD_NAME = "owner"
local pipeworks_path = minetest.get_modpath("pipeworks")

function inventory_quickmove_string(x,y)  
	return "button["..(x+0.3)..","..(y+2.1)..";2,0.8;inv_tochest;"..S("To Digtron").."]"..
	"button["..(x+0.3)..","..(y+4.1)..";2,0.8;inv_fromchest;"..S("To Inventory").."]"..
	"tooltip[inv_tochest;"..S("Move items from inventory to digtron").."]"..
	"tooltip[inv_fromchest;"..S("Move items from digtron to inventory").."]"..
	"list[current_name;quickmove;"..(x)..","..(y+3)..";1,1]"..
	"label["..(x+1)..","..(y+3)..";"..S("Item to move")..":\n("..S("Empty for all")..")]"
end 

local function update_infotext(pos)
	local meta = minetest.get_meta(pos)
    if not meta then return end
	local locked = meta:get_string(LOCKED_FLAG_NAME)
    if not locked or locked == "" then
        locked = "true"
    end
    local node = minetest.get_node(pos)
	local nodedef = minetest.registered_nodes[node.name]

	if locked == "true" then
		local infotext = S("Locked @1 (owned by @2)", nodedef.description, meta:get_string(OWNER_FIELD_NAME) or "<none>")
		meta:set_string("infotext", infotext)
    else
		meta:set_string("infotext", nodedef.description)
	end
end

local function owner_only_flag(meta, locked)
    if locked ~= "true" then
        return ""
    end
    local owner_only = meta:get_string(OWNER_ONLY_FLAG_NAME)
    if not owner_only or owner_only == "" then
        owner_only = "false"
    end
	local tooltip = "tooltip[owner_only;Only the owner can interact with this node \n even in shared areas]"
	return "checkbox[6.25,-0.25;owner_only;Owner Only;" .. owner_only .. "]" .. tooltip
end

local function inventory_locked_flag(pos)
	local meta = minetest.get_meta(pos)
	if not meta then return "" end

	local locked = meta:get_string(LOCKED_FLAG_NAME)
    if not locked or locked == "" then
        locked = "true"
    end
    update_infotext(pos)
	local tooltip = "tooltip[locked;Only people added in shared areas\ncan interact]"
	return "checkbox[4.5,-0.25;locked;Locked;" .. locked .. "]" .. owner_only_flag(meta, locked) .. tooltip
end

local function can_interact(player, pos)
	if not player or not pos then return false end
	local meta = minetest.get_meta(pos)
	local locked = meta and meta:get_string(LOCKED_FLAG_NAME) or ""
	if not locked or locked == "" then
		return true
	end
    if locked == "false" then
        return true
    end
    local owner = meta:get_string(OWNER_FIELD_NAME)
    if owner and player:get_player_name() == owner then
        return true
    end
    local owner_only = meta:get_string(OWNER_ONLY_FLAG_NAME)
	if owner_only == "true" then
		return false
	end
    if technic.can_interact(player, pos) then
        return true
    end
	return false
end

local inventory_formspec_string =
	"size[10.5,9.3]" ..
	default.gui_bg ..
	default.gui_bg_img ..
	default.gui_slots ..
	"label[0,0;" .. S("Digtron inventory items") .. "]" ..
	"list[current_name;main;0,0.6;8,4;]" ..
	"list[current_player;main;0,5.15;8,1;]" ..
	"list[current_player;main;0,6.38;8,3;8]" ..
	"listring[current_name;main]" ..
	"listring[current_player;main]" ..
	inventory_quickmove_string(8,4) ..
	default.get_hotbar_bg(0,5.15)

local inventory_formspec = function(pos, _)
	return inventory_formspec_string ..
	inventory_locked_flag(pos)
end

local function on_place(itemstack, placer, pointed_thing, formspec_func)
	local leftover, pos = minetest.item_place(itemstack, placer, pointed_thing)
	if pos then
		local meta = minetest.get_meta(pos)
		meta:set_string(OWNER_FIELD_NAME, placer:get_player_name() or "")
		meta:set_string("formspec", formspec_func(pos, meta))
	end
	return leftover, pos
end

local function on_receive_fields__locked(pos, fields, sender, formspec_func)
	if fields.locked then
		local meta = minetest.get_meta(pos)
		meta:set_string(LOCKED_FLAG_NAME, fields.locked)
		meta:set_string(OWNER_FIELD_NAME, sender:get_player_name() or "")
        meta:set_string("formspec", formspec_func(pos, meta))
	elseif fields.owner_only then
		local meta = minetest.get_meta(pos)
		meta:set_string(OWNER_ONLY_FLAG_NAME, fields.owner_only)
		meta:set_string("formspec", formspec_func(pos, meta))
	end
end

local function is_fuel_ready(listname,stack)
	if listname == "fuel" then
		if minetest.get_craft_result({method="fuel", width=1, items={stack}}).time ~= 0 then
			return true
		else
			return false
		end
	end
	return true
end
local function move_inv(frominv,frominv_list_name, toinv,toinv_list_name, filter)
	for i, v in ipairs(frominv:get_list(frominv_list_name) or {}) do
		if v:get_name() == filter or not filter then
            local name = v:get_name() or "air"
			if toinv:room_for_item(toinv_list_name, v) and is_fuel_ready(toinv_list_name,v) and minetest.get_item_group(name, "bag") == 0 then
				local leftover = toinv:add_item(toinv_list_name, v)

				frominv:remove_item(frominv_list_name, v)

				if leftover
				and not leftover:is_empty() then
					frominv:add_item(frominv_list_name, v)
				end
			end
		end
	end
end

-- ty to technic_chests\ code that is copy/past here and modified
function inv_move(pos, from_list, from_index, to_list, to_index, count, player)
	local meta = minetest.get_meta(pos)
	local inv = meta:get_inventory()
    local stack = inv:get_stack(from_list, from_index)
	if not can_interact(player, pos) then return 0 end
	if from_list == "quickmove" then
		local stack_copy = inv:get_stack(to_list, to_index)
		inv:set_stack(to_list, to_index, stack_copy)
		inv:set_stack(from_list, from_index, ItemStack(""))
		meta:set_string("item", "")
		return 0
	elseif from_list == "main" and to_list == "quickmove" then
		local stack_copy = ItemStack(stack)
		stack_copy:set_count(1)
		inv:set_stack(to_list, to_index, stack_copy)
		meta:set_string("item", tostring(stack_copy:get_name()))
		return 0
	end
	return stack:get_count()
end

function inv_put(pos, listname, index, stack, player)
	if not can_interact(player, pos) then return 0 end

	if minetest.get_item_group(stack:get_name(), "bag") > 0 then
		return 0
	end
	-- Only allow fuel items to be placed in fuel
	if listname == "fuel" then
		if minetest.get_craft_result({ method = "fuel", width = 1, items = { stack } }).time ~= 0 then
			return stack:get_count()
		else
			return 0
		end
	elseif listname == "quickmove" then
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local stack_copy = ItemStack(stack)
		stack_copy:set_count(1)
		inv:set_stack(listname, index, stack_copy)
		meta:set_string("item", tostring(stack:get_name()))
		return 0
	end

	return stack:get_count()
end

function inv_take(pos, listname, index, stack, player)
	if not can_interact(player, pos) then return 0 end

    if listname == "quickmove" then
        local meta = minetest.get_meta(pos)
        local inv = meta:get_inventory()
        inv:set_stack(listname, index, ItemStack(""))
        meta:set_string("item", "")
        return 0
    end
    return stack:get_count()
end

-- Storage buffer. Builder nodes draw from this inventory and digger nodes deposit into it.
-- Note that inventories are digtron group 2.
minetest.register_node("digtron:inventory", {
	description = S("Digtron Inventory Storage"),
	_doc_items_longdesc = digtron.doc.inventory_longdesc,
	_doc_items_usagehelp = digtron.doc.inventory_usagehelp,
	_digtron_formspec = inventory_formspec,
	groups = {cracky = 3, oddly_breakable_by_hand=3, digtron = 2, tubedevice = 1, tubedevice_receiver = 1},
	drop = "digtron:inventory",
	sounds = digtron.metal_sounds,
	paramtype2= "facedir",
	drawtype = "nodebox",
	node_box = {
        type = "fixed",
        fixed = {
            {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
        },
    },
	paramtype = "light",
	is_ground_content = false,
	tiles = {
		"digtron_plate.png^digtron_crossbrace.png",
		"digtron_plate.png^digtron_crossbrace.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_storage.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_storage.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_storage.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_storage.png",
		},

	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", inventory_formspec(pos, meta))
		local inv = meta:get_inventory()
		inv:set_size("main", 8*4)
		inv:set_size("quickmove", 1*1)
	end,
    on_place = function(itemstack, placer, pointed_thing)
		return on_place(itemstack, placer, pointed_thing, inventory_formspec)
	end,
	can_dig = function(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		return inv:is_empty("main")
	end,

	allow_metadata_inventory_move = inv_move,
	allow_metadata_inventory_put = inv_put,
	allow_metadata_inventory_take = inv_take,
    on_receive_fields = function(pos, formname, fields, sender)
        if not can_interact(sender, pos) then
            return
        end
		on_receive_fields__locked(pos, fields, sender, inventory_formspec)
		if fields.inv_tochest then
			local player_inv = sender:get_inventory()
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			if meta:get_string("item") == "" then
				minetest.log("action", sender:get_player_name().." moves all inventory contents to digtron at "..minetest.pos_to_string(pos))
				move_inv(player_inv,"main",  inv,"main", nil)
			else
				minetest.log("action", sender:get_player_name().." moves all "..meta:get_string("item").." in inventory to digtron at "..minetest.pos_to_string(pos))
				move_inv(player_inv,"main",  inv,"main",  meta:get_string("item"))
			end
		end
		if fields.inv_fromchest then
			local player_inv = sender:get_inventory()
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			if meta:get_string("item") == "" then
				minetest.log("action", sender:get_player_name().." moves all contents to inventory from digtron at "..minetest.pos_to_string(pos))
				move_inv(inv,"main",  player_inv,"main",  nil)
			else
				minetest.log("action", sender:get_player_name().." moves all "..meta:get_string("item").." to inventory from digtron at "..minetest.pos_to_string(pos))
				move_inv(inv,"main",  player_inv,"main",  meta:get_string("item"))
			end
		end
		if fields.quit then
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			inv:set_list("quickmove", {})
			meta:set_string("item", "")
		end
	end,



		
	-- Pipeworks compatibility
	----------------------------------------------------------------

	tube = (function() if pipeworks_path then return {
		insert_object = function(pos, _, stack)
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			return inv:add_item("main", stack)
		end,
		can_insert = function(pos, _, stack)
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			return inv:room_for_item("main", stack)
		end,
		input_inventory = "main",
		connect_sides = {left = 1, right = 1, back = 1, front = 1, bottom = 1, top = 1}
	} end end)(),

	after_place_node = (function() if pipeworks_path then return pipeworks.after_place end end)(),
	after_dig_node = (function() if pipeworks_path then return pipeworks.after_dig end end)()
})

local fuelstore_formspec_string = 
	"size[10.5,9.3]" ..
	default.gui_bg ..
	default.gui_bg_img ..
	default.gui_slots ..
	"label[0,0;" .. S("Fuel items") .. "]" ..
	"list[current_name;fuel;0,0.6;8,4;]" ..
	"list[current_player;main;0,5.15;8,1;]" ..
	"list[current_player;main;0,6.38;8,3;8]" ..
	"listring[current_name;fuel]" ..
	"listring[current_player;main]" ..
	inventory_quickmove_string(8,4) ..
	default.get_hotbar_bg(0,5.15)

local fuelstore_formspec = function(pos, _)
	return fuelstore_formspec_string ..
	inventory_locked_flag(pos)
end

-- Fuel storage. Controller node draws fuel from here.
-- Note that fuel stores are digtron group 5.
minetest.register_node("digtron:fuelstore", {
	description = S("Digtron Fuel Storage"),
	_doc_items_longdesc = digtron.doc.fuelstore_longdesc,
	_doc_items_usagehelp = digtron.doc.fuelstore_usagehelp,
	_digtron_formspec = fuelstore_formspec,
	groups = {cracky = 3,  oddly_breakable_by_hand=3, digtron = 5, tubedevice = 1, tubedevice_receiver = 1},
	drop = "digtron:fuelstore",
	sounds = digtron.metal_sounds,
	paramtype2= "facedir",
	drawtype = "nodebox",
	node_box = {
        type = "fixed",
        fixed = {
            {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
        },
    },
	paramtype = "light",
	is_ground_content = false,
	tiles = {
		"digtron_plate.png^digtron_crossbrace.png^digtron_flammable.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_flammable.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_flammable.png^digtron_storage.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_flammable.png^digtron_storage.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_flammable.png^digtron_storage.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_flammable.png^digtron_storage.png",
		},

	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", fuelstore_formspec(pos, meta))
		local inv = meta:get_inventory()
		inv:set_size("fuel", 8*4)
		inv:set_size("quickmove", 1*1)
	end,
	on_place = function(itemstack, placer, pointed_thing)
		return on_place(itemstack, placer, pointed_thing, fuelstore_formspec)
	end,
	can_dig = function(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		return inv:is_empty("fuel")
	end,

	allow_metadata_inventory_move = inv_move,
	allow_metadata_inventory_put = inv_put,
	allow_metadata_inventory_take = inv_take,
	on_receive_fields = function(pos, formname, fields, sender)
		if not can_interact(sender, pos) then
			return
		end
		on_receive_fields__locked(pos, fields, sender, fuelstore_formspec)
		if fields.inv_tochest then
			local player_inv = sender:get_inventory()
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			if meta:get_string("item") == "" then
				minetest.log("action", sender:get_player_name().." moves all inventory contents to digtron at "..minetest.pos_to_string(pos))
				move_inv(player_inv,"main",  inv,"fuel",  nil)
			else
				minetest.log("action", sender:get_player_name().." moves all "..meta:get_string("item").." in inventory to digtron at "..minetest.pos_to_string(pos))
				move_inv(player_inv,"main",  inv,"fuel",  meta:get_string("item"))
			end
		end
		if fields.inv_fromchest then
			local player_inv = sender:get_inventory()
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			if meta:get_string("item") == "" then
				minetest.log("action", sender:get_player_name().." moves all contents to inventory from digtron at "..minetest.pos_to_string(pos))
				move_inv(inv,"fuel",  player_inv,"main",  nil)
			else
				minetest.log("action", sender:get_player_name().." moves all "..meta:get_string("item").." to inventory from digtron at "..minetest.pos_to_string(pos))
				move_inv(inv,"fuel",  player_inv,"main",  meta:get_string("item"))
			end
		end
		if fields.quit then
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			inv:set_list("quickmove", {})
			meta:set_string("item", "")
		end
	end,
		
	-- Pipeworks compatibility
	----------------------------------------------------------------

	tube = (function() if pipeworks_path then return {
		insert_object = function(pos, _, stack)
			if minetest.get_craft_result({method="fuel", width=1, items={stack}}).time ~= 0 then
				local meta = minetest.get_meta(pos)
				local inv = meta:get_inventory()
				return inv:add_item("fuel", stack)
			end
			return stack
		end,
		can_insert = function(pos, _, stack)
			if minetest.get_craft_result({method="fuel", width=1, items={stack}}).time ~= 0 then
				local meta = minetest.get_meta(pos)
				local inv = meta:get_inventory()
				return inv:room_for_item("fuel", stack)
			end
			return false
		end,
		input_inventory = "fuel",
		connect_sides = {left = 1, right = 1, back = 1, front = 1, bottom = 1, top = 1}
	} end end)(),

	after_place_node = (function() if pipeworks_path then return pipeworks.after_place end end)(),
	after_dig_node = (function() if pipeworks_path then return pipeworks.after_dig end end)()
})

local combined_storage_formspec_string =
	"size[10.5,9.9]" ..
	default.gui_bg ..
	default.gui_bg_img ..
	default.gui_slots ..
	"label[0,0;" .. S("Digtron inventory items and fuel") .. "]" ..
	"list[current_name;main;0,0.6;8,3;]" ..
	"label[0,3.5;" .. S("Fuel items") .. "]" ..
	"list[current_name;fuel;0,4.1;8,1;]" ..
	"list[current_player;main;0,5.75;8,1;]" ..
	"list[current_player;main;0,6.98;8,3;8]" ..
	"listring[current_name;main]" ..
	"listring[current_player;main]" ..
	inventory_quickmove_string(8,4) ..
	default.get_hotbar_bg(0,5.75)

local combined_storage_formspec = function(pos, _)
	return combined_storage_formspec_string ..
	inventory_locked_flag(pos)
end

-- Combined storage. Group 6 has both an inventory and a fuel store
minetest.register_node("digtron:combined_storage", {
	description = S("Digtron Combined Storage"),
	_doc_items_longdesc = digtron.doc.combined_storage_longdesc,
    _doc_items_usagehelp = digtron.doc.combined_storage_usagehelp,
	_digtron_formspec = combined_storage_formspec,
	groups = {cracky = 3,  oddly_breakable_by_hand=3, digtron = 6, tubedevice = 1, tubedevice_receiver = 1},
	drop = "digtron:combined_storage",
	sounds = digtron.metal_sounds,
	paramtype2= "facedir",
	drawtype = "nodebox",
	node_box = {
        type = "fixed",
        fixed = {
            {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
        },
    },
	paramtype = "light",
	is_ground_content = false,
	tiles = {
		"digtron_plate.png^digtron_crossbrace.png^digtron_flammable_small.png^[transformR180^digtron_flammable_small.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_flammable_small.png^[transformR180^digtron_flammable_small.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_flammable_small.png^digtron_storage.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_flammable_small.png^digtron_storage.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_flammable_small.png^digtron_storage.png",
		"digtron_plate.png^digtron_crossbrace.png^digtron_flammable_small.png^digtron_storage.png",
		},
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", combined_storage_formspec(pos, meta))
		local inv = meta:get_inventory()
		inv:set_size("main", 8*3)
		inv:set_size("fuel", 8*1)
		inv:set_size("quickmove", 1*1)
	end,
	on_place = function(itemstack, placer, pointed_thing)
		return on_place(itemstack, placer, pointed_thing, combined_storage_formspec)
	end,
	-- Only allow fuel items to be placed in fuel
	allow_metadata_inventory_put = function(_, listname, _, stack)
		if listname == "fuel" then
			if minetest.get_craft_result({method="fuel", width=1, items={stack}}).time ~= 0 then
				return stack:get_count()
			else
				return 0
			end
		end
		return stack:get_count() -- otherwise, allow all drops
	end,

	allow_metadata_inventory_move = function(pos, from_list, from_index, to_list, _, count)
		if to_list == "main" then
			return count
		end

		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local stack = inv:get_stack(from_list, from_index)
		if minetest.get_craft_result({method="fuel", width=1, items={stack}}).time ~= 0 then
			return stack:get_count()
		end
		return 0
	end,

	can_dig = function(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		return inv:is_empty("fuel") and inv:is_empty("main")
	end,

	allow_metadata_inventory_move = inv_move,
	allow_metadata_inventory_put = inv_put,
	allow_metadata_inventory_take = inv_take,
	on_receive_fields = function(pos, formname, fields, sender)
		if not can_interact(sender, pos) then
			return
		end
		on_receive_fields__locked(pos, fields, sender, combined_storage_formspec)
		if fields.inv_tochest then
			local player_inv = sender:get_inventory()
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			if meta:get_string("item") == "" then
				minetest.log("action", sender:get_player_name().." moves all inventory contents to digtron at "..minetest.pos_to_string(pos))
				move_inv(player_inv,"main",  inv,"main",  nil)
			else
				minetest.log("action", sender:get_player_name().." moves all "..meta:get_string("item").." in inventory to digtron at "..minetest.pos_to_string(pos))
				move_inv(player_inv,"main",  inv,"main",  meta:get_string("item"))
			end
		end
		if fields.inv_fromchest then
			local player_inv = sender:get_inventory()
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			if meta:get_string("item") == "" then
				minetest.log("action", sender:get_player_name().." moves all contents to inventory from digtron at "..minetest.pos_to_string(pos))
				move_inv(inv,"main",  player_inv,"main",  nil)
			else
				minetest.log("action", sender:get_player_name().." moves all "..meta:get_string("item").." to inventory from digtron at "..minetest.pos_to_string(pos))
				move_inv(inv,"main",  player_inv,"main",  meta:get_string("item"))
			end
		end
		if fields.quit then
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			inv:set_list("quickmove", {})
			meta:set_string("item", "")
		end
	end,
		
	-- Pipeworks compatibility
	----------------------------------------------------------------
	tube = (function() if pipeworks_path then return {
		insert_object = function(pos, _, stack, direction)
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			if minetest.get_craft_result({method="fuel", width=1, items={stack}}).time ~= 0 and direction.y == 1 then
				return inv:add_item("fuel", stack)
			end
			return inv:add_item("main", stack)
		end,
		can_insert = function(pos, _, stack, direction)
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			if minetest.get_craft_result({method="fuel", width=1, items={stack}}).time ~= 0 and direction.y == 1 then
				return inv:room_for_item("fuel", stack)
			end
			return inv:room_for_item("main", stack)
		end,
		input_inventory = "main",
		connect_sides = {left = 1, right = 1, back = 1, front = 1, bottom = 1, top = 1}
	} end end)(),

	after_place_node = (function() if pipeworks_path then return pipeworks.after_place end end)(),
	after_dig_node = (function() if pipeworks_path then return pipeworks.after_dig end end)()
})

-- Hopper compatibility
if minetest.get_modpath("hopper") and hopper ~= nil and hopper.add_container ~= nil then
	hopper:add_container({
		{"top", "digtron:inventory", "main"},
		{"bottom", "digtron:inventory", "main"},
		{"side", "digtron:inventory", "main"},

		{"top", "digtron:fuelstore", "fuel"},
		{"bottom", "digtron:fuelstore", "fuel"},
		{"side", "digtron:fuelstore", "fuel"},

		{"top", "digtron:combined_storage", "main"},
		{"bottom", "digtron:combined_storage", "main"},
		{"side", "digtron:combined_storage", "fuel"},
	})
end
